#include <stdio.h>

void main(){
    char a[3]={'a','b','c'};
    #ifdef dothis    
    for(int i=0;i<3;i++)
    {
        printf("%c",a[i]);        
    }    
    printf("\r\ndo this ok...");
    #endif

    #ifdef dothat
    for(int i=0;i<=3;i++)
    {
        printf("%c",a[i]);        
    }    
    printf("\r\ndo that ok...");
    #endif
}