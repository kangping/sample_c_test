#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct test_mem {
        int test_a;
        char test_flag;
        int test_end;
} TEST_MEM;

typedef struct test_mem_cnt {
        int test_a;
        char test_flag;
        int cnt;
        int test_end;
} TEST_MEM_WITH_CNT;

void complex_buggy(void)
{
     int arr[4] = { 0 };
     int idx = 2;
     int flag = 0;
     char *str = "hello";
     int map[] = { 1, 2, 3, 5, 8, 13 };

     flag = map[str[1] - 'a'];

     if (flag > 7) {
          idx += strlen(str);
     }

     arr[idx] = 1;	// Overflow
}

int forward(int *skb, int *skb0)
{
     if (skb0 == skb)
     {
          skb0 = malloc(4);
     }

     free(skb);
     return 0;
}

void double_free_demo()
{
     int *skb = malloc(4);
     int *skb0 = skb;

     forward(skb, skb0);

     free(skb0);	// Double Fee
}

void mem_out_of_range(int num)
{
        TEST_MEM* pTestMem = NULL;
        TEST_MEM_WITH_CNT* pTestMemCnt = NULL;
        char* buf = NULL;
        int i = 0;
        int len = 0;
        len = sizeof(TEST_MEM) + sizeof(TEST_MEM_WITH_CNT) * num;
        buf = (char*)malloc(len);
        if (buf != NULL)
        {
                pTestMem = (TEST_MEM *)buf;
                pTestMem->test_a = 1;
                pTestMem->test_flag = 1;
                pTestMem->test_end = 1;

                for (i = 0; i < num; i++)
                {
                        pTestMemCnt = (TEST_MEM_WITH_CNT*)(buf + i * sizeof(TEST_MEM));
                        pTestMemCnt->test_a = 1;
                        pTestMemCnt->test_flag = 1;
                        pTestMemCnt->cnt = 1;
                        pTestMemCnt->test_end = 1;
                }

                pTestMemCnt = (TEST_MEM_WITH_CNT*)(buf + i * sizeof(TEST_MEM));
                pTestMemCnt->test_a = 1;
                pTestMemCnt->test_flag = 1;
                pTestMemCnt->cnt = 1;
                pTestMemCnt->test_end = 1;        // heap-buffer-overflow

                free(buf);
        }

        buf = NULL;
}

#define MAX_MAC_COUNT        32                /* max mac count in frame */
#define ME_T_MACADDR        5
#define STR_MAC_LEN                18

typedef struct _MSG_ELEMENT_HDR
{
        unsigned short type;                /* me type */
        unsigned short length;                /* me data length */
        unsigned char  data[0];                /* me data */
} MSG_ELEMENT_HDR;

MSG_ELEMENT_HDR* parse_msg_element(MSG_ELEMENT_HDR *meh, int total_len, unsigned short me_type)
{
        int             offset   = 0;
        MSG_ELEMENT_HDR *tmp_meh = NULL;
        unsigned short  type     = 0;
        unsigned short  length   = 0;

        if ((NULL == meh) || (total_len <= 0))
        {
                return NULL;
        }

        while (total_len > 0)
        {
                tmp_meh = (MSG_ELEMENT_HDR *)((char *)meh + offset);
                type   = tmp_meh->type;
                length = tmp_meh->length;
                if (type == me_type)
                {
                        return tmp_meh;
                }
                offset    += sizeof(MSG_ELEMENT_HDR) + length;
                total_len -= sizeof(MSG_ELEMENT_HDR) + length;
        }

        return NULL;
}

int copy_msg_element(MSG_ELEMENT_HDR *meh, char *buf, unsigned int size)
{
        unsigned int len = 0;

        if ((NULL == meh) || (NULL == buf))
        {
                return -1;
        }

        len = (unsigned int)(meh->length);

        if (len > size)
        {
                return -1;
        }

        memcpy(buf, meh->data, len);	// Write Overflow

        return 0;
}

int parse_frame(unsigned char *payload, unsigned short payloadLen)
{
        char            mac[MAX_MAC_COUNT][STR_MAC_LEN] = {0};
        MSG_ELEMENT_HDR *tmpMeh  = NULL;
        int                                count          = 0;
        int                         length          = 0;

        if ((NULL == payload) || (0 == payloadLen))
        {
                return -1;
        }

        tmpMeh = (MSG_ELEMENT_HDR *)payload;
        length = payloadLen;

        /* search for macaddr in frame */
        while (NULL != tmpMeh)
        {
                if (count >= MAX_MAC_COUNT)
                {
                        return -1;
                }
                tmpMeh = parse_msg_element(tmpMeh, length, ME_T_MACADDR);
                if (NULL == tmpMeh)
                {
                        break;
                }
                if (-1 == copy_msg_element(tmpMeh, mac[0] + count * STR_MAC_LEN, tmpMeh->length))
                {
                        return -1;
                }
                count++;
                tmpMeh = (MSG_ELEMENT_HDR *)((char *)tmpMeh + sizeof(MSG_ELEMENT_HDR) + tmpMeh->length);
                length = length - sizeof(MSG_ELEMENT_HDR) - tmpMeh->length;
        }
        return 0;
}

void memory_overstep_boundary()
{
        unsigned char ph[] = {0x05, 0x00, 0x08, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x05, 0x00, 0x01, 0x05, 0x00, 0x01};
        unsigned char *payload = ph;
        unsigned short payloadLen = 12;
        parse_frame(payload, payloadLen);
}

int main(void)
{
    complex_buggy();
    double_free_demo();
    mem_out_of_range(0);
    memory_overstep_boundary();

    return 0;
}

