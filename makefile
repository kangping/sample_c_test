all:buggy.exe
buggy.exe: buggy.c
	gcc -o buggy.exe buggy.c

clean:
	rm buggy.exe
